//  This file was automatically generated and should not be edited.

import AWSAppSync

public struct CreateTaskInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID? = nil, assigned: String? = nil, beagonotes: String? = nil, due: String? = nil, householdid: String, name: String? = nil, status: Int? = nil, usernotes: String? = nil) {
    graphQLMap = ["id": id, "assigned": assigned, "beagonotes": beagonotes, "due": due, "householdid": householdid, "name": name, "status": status, "usernotes": usernotes]
  }

  public var id: GraphQLID? {
    get {
      return graphQLMap["id"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var assigned: String? {
    get {
      return graphQLMap["assigned"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "assigned")
    }
  }

  public var beagonotes: String? {
    get {
      return graphQLMap["beagonotes"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "beagonotes")
    }
  }

  public var due: String? {
    get {
      return graphQLMap["due"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "due")
    }
  }

  public var householdid: String {
    get {
      return graphQLMap["householdid"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "householdid")
    }
  }

  public var name: String? {
    get {
      return graphQLMap["name"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var status: Int? {
    get {
      return graphQLMap["status"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "status")
    }
  }

  public var usernotes: String? {
    get {
      return graphQLMap["usernotes"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "usernotes")
    }
  }
}

public struct UpdateTaskInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID, assigned: String? = nil, beagonotes: String? = nil, due: String? = nil, householdid: String? = nil, name: String? = nil, status: Int? = nil, usernotes: String? = nil) {
    graphQLMap = ["id": id, "assigned": assigned, "beagonotes": beagonotes, "due": due, "householdid": householdid, "name": name, "status": status, "usernotes": usernotes]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var assigned: String? {
    get {
      return graphQLMap["assigned"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "assigned")
    }
  }

  public var beagonotes: String? {
    get {
      return graphQLMap["beagonotes"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "beagonotes")
    }
  }

  public var due: String? {
    get {
      return graphQLMap["due"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "due")
    }
  }

  public var householdid: String? {
    get {
      return graphQLMap["householdid"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "householdid")
    }
  }

  public var name: String? {
    get {
      return graphQLMap["name"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var status: Int? {
    get {
      return graphQLMap["status"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "status")
    }
  }

  public var usernotes: String? {
    get {
      return graphQLMap["usernotes"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "usernotes")
    }
  }
}

public struct DeleteTaskInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID? = nil) {
    graphQLMap = ["id": id]
  }

  public var id: GraphQLID? {
    get {
      return graphQLMap["id"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }
}

public struct CreatePersonInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID? = nil, householdid: String, name: String? = nil) {
    graphQLMap = ["id": id, "householdid": householdid, "name": name]
  }

  public var id: GraphQLID? {
    get {
      return graphQLMap["id"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var householdid: String {
    get {
      return graphQLMap["householdid"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "householdid")
    }
  }

  public var name: String? {
    get {
      return graphQLMap["name"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }
}

public struct UpdatePersonInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID, householdid: String? = nil, name: String? = nil) {
    graphQLMap = ["id": id, "householdid": householdid, "name": name]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var householdid: String? {
    get {
      return graphQLMap["householdid"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "householdid")
    }
  }

  public var name: String? {
    get {
      return graphQLMap["name"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }
}

public struct DeletePersonInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID? = nil) {
    graphQLMap = ["id": id]
  }

  public var id: GraphQLID? {
    get {
      return graphQLMap["id"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }
}

public struct ModelTaskFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: ModelIDFilterInput? = nil, assigned: ModelStringFilterInput? = nil, beagonotes: ModelStringFilterInput? = nil, due: ModelStringFilterInput? = nil, householdid: ModelStringFilterInput? = nil, name: ModelStringFilterInput? = nil, status: ModelIntFilterInput? = nil, usernotes: ModelStringFilterInput? = nil, and: [ModelTaskFilterInput?]? = nil, or: [ModelTaskFilterInput?]? = nil, not: ModelTaskFilterInput? = nil) {
    graphQLMap = ["id": id, "assigned": assigned, "beagonotes": beagonotes, "due": due, "householdid": householdid, "name": name, "status": status, "usernotes": usernotes, "and": and, "or": or, "not": not]
  }

  public var id: ModelIDFilterInput? {
    get {
      return graphQLMap["id"] as! ModelIDFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var assigned: ModelStringFilterInput? {
    get {
      return graphQLMap["assigned"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "assigned")
    }
  }

  public var beagonotes: ModelStringFilterInput? {
    get {
      return graphQLMap["beagonotes"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "beagonotes")
    }
  }

  public var due: ModelStringFilterInput? {
    get {
      return graphQLMap["due"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "due")
    }
  }

  public var householdid: ModelStringFilterInput? {
    get {
      return graphQLMap["householdid"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "householdid")
    }
  }

  public var name: ModelStringFilterInput? {
    get {
      return graphQLMap["name"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var status: ModelIntFilterInput? {
    get {
      return graphQLMap["status"] as! ModelIntFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "status")
    }
  }

  public var usernotes: ModelStringFilterInput? {
    get {
      return graphQLMap["usernotes"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "usernotes")
    }
  }

  public var and: [ModelTaskFilterInput?]? {
    get {
      return graphQLMap["and"] as! [ModelTaskFilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "and")
    }
  }

  public var or: [ModelTaskFilterInput?]? {
    get {
      return graphQLMap["or"] as! [ModelTaskFilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "or")
    }
  }

  public var not: ModelTaskFilterInput? {
    get {
      return graphQLMap["not"] as! ModelTaskFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "not")
    }
  }
}

public struct ModelIDFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ne: GraphQLID? = nil, eq: GraphQLID? = nil, le: GraphQLID? = nil, lt: GraphQLID? = nil, ge: GraphQLID? = nil, gt: GraphQLID? = nil, contains: GraphQLID? = nil, notContains: GraphQLID? = nil, between: [GraphQLID?]? = nil, beginsWith: GraphQLID? = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between, "beginsWith": beginsWith]
  }

  public var ne: GraphQLID? {
    get {
      return graphQLMap["ne"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: GraphQLID? {
    get {
      return graphQLMap["eq"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: GraphQLID? {
    get {
      return graphQLMap["le"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: GraphQLID? {
    get {
      return graphQLMap["lt"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: GraphQLID? {
    get {
      return graphQLMap["ge"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: GraphQLID? {
    get {
      return graphQLMap["gt"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: GraphQLID? {
    get {
      return graphQLMap["contains"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: GraphQLID? {
    get {
      return graphQLMap["notContains"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: [GraphQLID?]? {
    get {
      return graphQLMap["between"] as! [GraphQLID?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }

  public var beginsWith: GraphQLID? {
    get {
      return graphQLMap["beginsWith"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "beginsWith")
    }
  }
}

public struct ModelStringFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ne: String? = nil, eq: String? = nil, le: String? = nil, lt: String? = nil, ge: String? = nil, gt: String? = nil, contains: String? = nil, notContains: String? = nil, between: [String?]? = nil, beginsWith: String? = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between, "beginsWith": beginsWith]
  }

  public var ne: String? {
    get {
      return graphQLMap["ne"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: String? {
    get {
      return graphQLMap["eq"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: String? {
    get {
      return graphQLMap["le"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: String? {
    get {
      return graphQLMap["lt"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: String? {
    get {
      return graphQLMap["ge"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: String? {
    get {
      return graphQLMap["gt"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: String? {
    get {
      return graphQLMap["contains"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: String? {
    get {
      return graphQLMap["notContains"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: [String?]? {
    get {
      return graphQLMap["between"] as! [String?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }

  public var beginsWith: String? {
    get {
      return graphQLMap["beginsWith"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "beginsWith")
    }
  }
}

public struct ModelIntFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ne: Int? = nil, eq: Int? = nil, le: Int? = nil, lt: Int? = nil, ge: Int? = nil, gt: Int? = nil, contains: Int? = nil, notContains: Int? = nil, between: [Int?]? = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between]
  }

  public var ne: Int? {
    get {
      return graphQLMap["ne"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: Int? {
    get {
      return graphQLMap["eq"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: Int? {
    get {
      return graphQLMap["le"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: Int? {
    get {
      return graphQLMap["lt"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: Int? {
    get {
      return graphQLMap["ge"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: Int? {
    get {
      return graphQLMap["gt"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: Int? {
    get {
      return graphQLMap["contains"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: Int? {
    get {
      return graphQLMap["notContains"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: [Int?]? {
    get {
      return graphQLMap["between"] as! [Int?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }
}

public struct ModelPersonFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: ModelIDFilterInput? = nil, householdid: ModelStringFilterInput? = nil, name: ModelStringFilterInput? = nil, and: [ModelPersonFilterInput?]? = nil, or: [ModelPersonFilterInput?]? = nil, not: ModelPersonFilterInput? = nil) {
    graphQLMap = ["id": id, "householdid": householdid, "name": name, "and": and, "or": or, "not": not]
  }

  public var id: ModelIDFilterInput? {
    get {
      return graphQLMap["id"] as! ModelIDFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var householdid: ModelStringFilterInput? {
    get {
      return graphQLMap["householdid"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "householdid")
    }
  }

  public var name: ModelStringFilterInput? {
    get {
      return graphQLMap["name"] as! ModelStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var and: [ModelPersonFilterInput?]? {
    get {
      return graphQLMap["and"] as! [ModelPersonFilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "and")
    }
  }

  public var or: [ModelPersonFilterInput?]? {
    get {
      return graphQLMap["or"] as! [ModelPersonFilterInput?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "or")
    }
  }

  public var not: ModelPersonFilterInput? {
    get {
      return graphQLMap["not"] as! ModelPersonFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "not")
    }
  }
}

public final class CreateTaskMutation: GraphQLMutation {
  public static let operationString =
    "mutation CreateTask($input: CreateTaskInput!) {\n  createTask(input: $input) {\n    __typename\n    id\n    assigned\n    beagonotes\n    due\n    householdid\n    name\n    status\n    usernotes\n  }\n}"

  public var input: CreateTaskInput

  public init(input: CreateTaskInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createTask", arguments: ["input": GraphQLVariable("input")], type: .object(CreateTask.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createTask: CreateTask? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createTask": createTask.flatMap { $0.snapshot }])
    }

    public var createTask: CreateTask? {
      get {
        return (snapshot["createTask"] as? Snapshot).flatMap { CreateTask(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "createTask")
      }
    }

    public struct CreateTask: GraphQLSelectionSet {
      public static let possibleTypes = ["Task"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("assigned", type: .scalar(String.self)),
        GraphQLField("beagonotes", type: .scalar(String.self)),
        GraphQLField("due", type: .scalar(String.self)),
        GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(Int.self)),
        GraphQLField("usernotes", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, assigned: String? = nil, beagonotes: String? = nil, due: String? = nil, householdid: String, name: String? = nil, status: Int? = nil, usernotes: String? = nil) {
        self.init(snapshot: ["__typename": "Task", "id": id, "assigned": assigned, "beagonotes": beagonotes, "due": due, "householdid": householdid, "name": name, "status": status, "usernotes": usernotes])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var assigned: String? {
        get {
          return snapshot["assigned"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "assigned")
        }
      }

      public var beagonotes: String? {
        get {
          return snapshot["beagonotes"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "beagonotes")
        }
      }

      public var due: String? {
        get {
          return snapshot["due"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "due")
        }
      }

      public var householdid: String {
        get {
          return snapshot["householdid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "householdid")
        }
      }

      public var name: String? {
        get {
          return snapshot["name"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }

      public var status: Int? {
        get {
          return snapshot["status"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "status")
        }
      }

      public var usernotes: String? {
        get {
          return snapshot["usernotes"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "usernotes")
        }
      }
    }
  }
}

public final class UpdateTaskMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateTask($input: UpdateTaskInput!) {\n  updateTask(input: $input) {\n    __typename\n    id\n    assigned\n    beagonotes\n    due\n    householdid\n    name\n    status\n    usernotes\n  }\n}"

  public var input: UpdateTaskInput

  public init(input: UpdateTaskInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateTask", arguments: ["input": GraphQLVariable("input")], type: .object(UpdateTask.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateTask: UpdateTask? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateTask": updateTask.flatMap { $0.snapshot }])
    }

    public var updateTask: UpdateTask? {
      get {
        return (snapshot["updateTask"] as? Snapshot).flatMap { UpdateTask(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateTask")
      }
    }

    public struct UpdateTask: GraphQLSelectionSet {
      public static let possibleTypes = ["Task"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("assigned", type: .scalar(String.self)),
        GraphQLField("beagonotes", type: .scalar(String.self)),
        GraphQLField("due", type: .scalar(String.self)),
        GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(Int.self)),
        GraphQLField("usernotes", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, assigned: String? = nil, beagonotes: String? = nil, due: String? = nil, householdid: String, name: String? = nil, status: Int? = nil, usernotes: String? = nil) {
        self.init(snapshot: ["__typename": "Task", "id": id, "assigned": assigned, "beagonotes": beagonotes, "due": due, "householdid": householdid, "name": name, "status": status, "usernotes": usernotes])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var assigned: String? {
        get {
          return snapshot["assigned"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "assigned")
        }
      }

      public var beagonotes: String? {
        get {
          return snapshot["beagonotes"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "beagonotes")
        }
      }

      public var due: String? {
        get {
          return snapshot["due"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "due")
        }
      }

      public var householdid: String {
        get {
          return snapshot["householdid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "householdid")
        }
      }

      public var name: String? {
        get {
          return snapshot["name"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }

      public var status: Int? {
        get {
          return snapshot["status"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "status")
        }
      }

      public var usernotes: String? {
        get {
          return snapshot["usernotes"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "usernotes")
        }
      }
    }
  }
}

public final class DeleteTaskMutation: GraphQLMutation {
  public static let operationString =
    "mutation DeleteTask($input: DeleteTaskInput!) {\n  deleteTask(input: $input) {\n    __typename\n    id\n    assigned\n    beagonotes\n    due\n    householdid\n    name\n    status\n    usernotes\n  }\n}"

  public var input: DeleteTaskInput

  public init(input: DeleteTaskInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteTask", arguments: ["input": GraphQLVariable("input")], type: .object(DeleteTask.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(deleteTask: DeleteTask? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "deleteTask": deleteTask.flatMap { $0.snapshot }])
    }

    public var deleteTask: DeleteTask? {
      get {
        return (snapshot["deleteTask"] as? Snapshot).flatMap { DeleteTask(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "deleteTask")
      }
    }

    public struct DeleteTask: GraphQLSelectionSet {
      public static let possibleTypes = ["Task"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("assigned", type: .scalar(String.self)),
        GraphQLField("beagonotes", type: .scalar(String.self)),
        GraphQLField("due", type: .scalar(String.self)),
        GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(Int.self)),
        GraphQLField("usernotes", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, assigned: String? = nil, beagonotes: String? = nil, due: String? = nil, householdid: String, name: String? = nil, status: Int? = nil, usernotes: String? = nil) {
        self.init(snapshot: ["__typename": "Task", "id": id, "assigned": assigned, "beagonotes": beagonotes, "due": due, "householdid": householdid, "name": name, "status": status, "usernotes": usernotes])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var assigned: String? {
        get {
          return snapshot["assigned"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "assigned")
        }
      }

      public var beagonotes: String? {
        get {
          return snapshot["beagonotes"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "beagonotes")
        }
      }

      public var due: String? {
        get {
          return snapshot["due"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "due")
        }
      }

      public var householdid: String {
        get {
          return snapshot["householdid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "householdid")
        }
      }

      public var name: String? {
        get {
          return snapshot["name"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }

      public var status: Int? {
        get {
          return snapshot["status"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "status")
        }
      }

      public var usernotes: String? {
        get {
          return snapshot["usernotes"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "usernotes")
        }
      }
    }
  }
}

public final class CreatePersonMutation: GraphQLMutation {
  public static let operationString =
    "mutation CreatePerson($input: CreatePersonInput!) {\n  createPerson(input: $input) {\n    __typename\n    id\n    householdid\n    name\n  }\n}"

  public var input: CreatePersonInput

  public init(input: CreatePersonInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createPerson", arguments: ["input": GraphQLVariable("input")], type: .object(CreatePerson.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createPerson: CreatePerson? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createPerson": createPerson.flatMap { $0.snapshot }])
    }

    public var createPerson: CreatePerson? {
      get {
        return (snapshot["createPerson"] as? Snapshot).flatMap { CreatePerson(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "createPerson")
      }
    }

    public struct CreatePerson: GraphQLSelectionSet {
      public static let possibleTypes = ["Person"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, householdid: String, name: String? = nil) {
        self.init(snapshot: ["__typename": "Person", "id": id, "householdid": householdid, "name": name])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var householdid: String {
        get {
          return snapshot["householdid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "householdid")
        }
      }

      public var name: String? {
        get {
          return snapshot["name"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }
    }
  }
}

public final class UpdatePersonMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdatePerson($input: UpdatePersonInput!) {\n  updatePerson(input: $input) {\n    __typename\n    id\n    householdid\n    name\n  }\n}"

  public var input: UpdatePersonInput

  public init(input: UpdatePersonInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updatePerson", arguments: ["input": GraphQLVariable("input")], type: .object(UpdatePerson.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updatePerson: UpdatePerson? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updatePerson": updatePerson.flatMap { $0.snapshot }])
    }

    public var updatePerson: UpdatePerson? {
      get {
        return (snapshot["updatePerson"] as? Snapshot).flatMap { UpdatePerson(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updatePerson")
      }
    }

    public struct UpdatePerson: GraphQLSelectionSet {
      public static let possibleTypes = ["Person"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, householdid: String, name: String? = nil) {
        self.init(snapshot: ["__typename": "Person", "id": id, "householdid": householdid, "name": name])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var householdid: String {
        get {
          return snapshot["householdid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "householdid")
        }
      }

      public var name: String? {
        get {
          return snapshot["name"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }
    }
  }
}

public final class DeletePersonMutation: GraphQLMutation {
  public static let operationString =
    "mutation DeletePerson($input: DeletePersonInput!) {\n  deletePerson(input: $input) {\n    __typename\n    id\n    householdid\n    name\n  }\n}"

  public var input: DeletePersonInput

  public init(input: DeletePersonInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deletePerson", arguments: ["input": GraphQLVariable("input")], type: .object(DeletePerson.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(deletePerson: DeletePerson? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "deletePerson": deletePerson.flatMap { $0.snapshot }])
    }

    public var deletePerson: DeletePerson? {
      get {
        return (snapshot["deletePerson"] as? Snapshot).flatMap { DeletePerson(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "deletePerson")
      }
    }

    public struct DeletePerson: GraphQLSelectionSet {
      public static let possibleTypes = ["Person"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, householdid: String, name: String? = nil) {
        self.init(snapshot: ["__typename": "Person", "id": id, "householdid": householdid, "name": name])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var householdid: String {
        get {
          return snapshot["householdid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "householdid")
        }
      }

      public var name: String? {
        get {
          return snapshot["name"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }
    }
  }
}

public final class GetTaskQuery: GraphQLQuery {
  public static let operationString =
    "query GetTask($id: ID!) {\n  getTask(id: $id) {\n    __typename\n    id\n    assigned\n    beagonotes\n    due\n    householdid\n    name\n    status\n    usernotes\n  }\n}"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getTask", arguments: ["id": GraphQLVariable("id")], type: .object(GetTask.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getTask: GetTask? = nil) {
      self.init(snapshot: ["__typename": "Query", "getTask": getTask.flatMap { $0.snapshot }])
    }

    public var getTask: GetTask? {
      get {
        return (snapshot["getTask"] as? Snapshot).flatMap { GetTask(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getTask")
      }
    }

    public struct GetTask: GraphQLSelectionSet {
      public static let possibleTypes = ["Task"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("assigned", type: .scalar(String.self)),
        GraphQLField("beagonotes", type: .scalar(String.self)),
        GraphQLField("due", type: .scalar(String.self)),
        GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(Int.self)),
        GraphQLField("usernotes", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, assigned: String? = nil, beagonotes: String? = nil, due: String? = nil, householdid: String, name: String? = nil, status: Int? = nil, usernotes: String? = nil) {
        self.init(snapshot: ["__typename": "Task", "id": id, "assigned": assigned, "beagonotes": beagonotes, "due": due, "householdid": householdid, "name": name, "status": status, "usernotes": usernotes])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var assigned: String? {
        get {
          return snapshot["assigned"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "assigned")
        }
      }

      public var beagonotes: String? {
        get {
          return snapshot["beagonotes"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "beagonotes")
        }
      }

      public var due: String? {
        get {
          return snapshot["due"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "due")
        }
      }

      public var householdid: String {
        get {
          return snapshot["householdid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "householdid")
        }
      }

      public var name: String? {
        get {
          return snapshot["name"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }

      public var status: Int? {
        get {
          return snapshot["status"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "status")
        }
      }

      public var usernotes: String? {
        get {
          return snapshot["usernotes"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "usernotes")
        }
      }
    }
  }
}

public final class ListTasksQuery: GraphQLQuery {
  public static let operationString =
    "query ListTasks($filter: ModelTaskFilterInput, $limit: Int, $nextToken: String) {\n  listTasks(filter: $filter, limit: $limit, nextToken: $nextToken) {\n    __typename\n    items {\n      __typename\n      id\n      assigned\n      beagonotes\n      due\n      householdid\n      name\n      status\n      usernotes\n    }\n    nextToken\n  }\n}"

  public var filter: ModelTaskFilterInput?
  public var limit: Int?
  public var nextToken: String?

  public init(filter: ModelTaskFilterInput? = nil, limit: Int? = nil, nextToken: String? = nil) {
    self.filter = filter
    self.limit = limit
    self.nextToken = nextToken
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "limit": limit, "nextToken": nextToken]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listTasks", arguments: ["filter": GraphQLVariable("filter"), "limit": GraphQLVariable("limit"), "nextToken": GraphQLVariable("nextToken")], type: .object(ListTask.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(listTasks: ListTask? = nil) {
      self.init(snapshot: ["__typename": "Query", "listTasks": listTasks.flatMap { $0.snapshot }])
    }

    public var listTasks: ListTask? {
      get {
        return (snapshot["listTasks"] as? Snapshot).flatMap { ListTask(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "listTasks")
      }
    }

    public struct ListTask: GraphQLSelectionSet {
      public static let possibleTypes = ["ModelTaskConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
        GraphQLField("nextToken", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(items: [Item?]? = nil, nextToken: String? = nil) {
        self.init(snapshot: ["__typename": "ModelTaskConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
        }
      }

      public var nextToken: String? {
        get {
          return snapshot["nextToken"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "nextToken")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes = ["Task"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("assigned", type: .scalar(String.self)),
          GraphQLField("beagonotes", type: .scalar(String.self)),
          GraphQLField("due", type: .scalar(String.self)),
          GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("status", type: .scalar(Int.self)),
          GraphQLField("usernotes", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, assigned: String? = nil, beagonotes: String? = nil, due: String? = nil, householdid: String, name: String? = nil, status: Int? = nil, usernotes: String? = nil) {
          self.init(snapshot: ["__typename": "Task", "id": id, "assigned": assigned, "beagonotes": beagonotes, "due": due, "householdid": householdid, "name": name, "status": status, "usernotes": usernotes])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var assigned: String? {
          get {
            return snapshot["assigned"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "assigned")
          }
        }

        public var beagonotes: String? {
          get {
            return snapshot["beagonotes"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "beagonotes")
          }
        }

        public var due: String? {
          get {
            return snapshot["due"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "due")
          }
        }

        public var householdid: String {
          get {
            return snapshot["householdid"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "householdid")
          }
        }

        public var name: String? {
          get {
            return snapshot["name"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "name")
          }
        }

        public var status: Int? {
          get {
            return snapshot["status"] as? Int
          }
          set {
            snapshot.updateValue(newValue, forKey: "status")
          }
        }

        public var usernotes: String? {
          get {
            return snapshot["usernotes"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "usernotes")
          }
        }
      }
    }
  }
}

public final class GetPersonQuery: GraphQLQuery {
  public static let operationString =
    "query GetPerson($id: ID!) {\n  getPerson(id: $id) {\n    __typename\n    id\n    householdid\n    name\n  }\n}"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getPerson", arguments: ["id": GraphQLVariable("id")], type: .object(GetPerson.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getPerson: GetPerson? = nil) {
      self.init(snapshot: ["__typename": "Query", "getPerson": getPerson.flatMap { $0.snapshot }])
    }

    public var getPerson: GetPerson? {
      get {
        return (snapshot["getPerson"] as? Snapshot).flatMap { GetPerson(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getPerson")
      }
    }

    public struct GetPerson: GraphQLSelectionSet {
      public static let possibleTypes = ["Person"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, householdid: String, name: String? = nil) {
        self.init(snapshot: ["__typename": "Person", "id": id, "householdid": householdid, "name": name])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var householdid: String {
        get {
          return snapshot["householdid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "householdid")
        }
      }

      public var name: String? {
        get {
          return snapshot["name"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }
    }
  }
}

public final class ListPersonsQuery: GraphQLQuery {
  public static let operationString =
    "query ListPersons($filter: ModelPersonFilterInput, $limit: Int, $nextToken: String) {\n  listPersons(filter: $filter, limit: $limit, nextToken: $nextToken) {\n    __typename\n    items {\n      __typename\n      id\n      householdid\n      name\n    }\n    nextToken\n  }\n}"

  public var filter: ModelPersonFilterInput?
  public var limit: Int?
  public var nextToken: String?

  public init(filter: ModelPersonFilterInput? = nil, limit: Int? = nil, nextToken: String? = nil) {
    self.filter = filter
    self.limit = limit
    self.nextToken = nextToken
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "limit": limit, "nextToken": nextToken]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listPersons", arguments: ["filter": GraphQLVariable("filter"), "limit": GraphQLVariable("limit"), "nextToken": GraphQLVariable("nextToken")], type: .object(ListPerson.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(listPersons: ListPerson? = nil) {
      self.init(snapshot: ["__typename": "Query", "listPersons": listPersons.flatMap { $0.snapshot }])
    }

    public var listPersons: ListPerson? {
      get {
        return (snapshot["listPersons"] as? Snapshot).flatMap { ListPerson(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "listPersons")
      }
    }

    public struct ListPerson: GraphQLSelectionSet {
      public static let possibleTypes = ["ModelPersonConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
        GraphQLField("nextToken", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(items: [Item?]? = nil, nextToken: String? = nil) {
        self.init(snapshot: ["__typename": "ModelPersonConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
        }
      }

      public var nextToken: String? {
        get {
          return snapshot["nextToken"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "nextToken")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes = ["Person"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .scalar(String.self)),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, householdid: String, name: String? = nil) {
          self.init(snapshot: ["__typename": "Person", "id": id, "householdid": householdid, "name": name])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var householdid: String {
          get {
            return snapshot["householdid"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "householdid")
          }
        }

        public var name: String? {
          get {
            return snapshot["name"] as? String
          }
          set {
            snapshot.updateValue(newValue, forKey: "name")
          }
        }
      }
    }
  }
}

public final class OnCreateTaskSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnCreateTask {\n  onCreateTask {\n    __typename\n    id\n    assigned\n    beagonotes\n    due\n    householdid\n    name\n    status\n    usernotes\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onCreateTask", type: .object(OnCreateTask.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onCreateTask: OnCreateTask? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onCreateTask": onCreateTask.flatMap { $0.snapshot }])
    }

    public var onCreateTask: OnCreateTask? {
      get {
        return (snapshot["onCreateTask"] as? Snapshot).flatMap { OnCreateTask(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onCreateTask")
      }
    }

    public struct OnCreateTask: GraphQLSelectionSet {
      public static let possibleTypes = ["Task"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("assigned", type: .scalar(String.self)),
        GraphQLField("beagonotes", type: .scalar(String.self)),
        GraphQLField("due", type: .scalar(String.self)),
        GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(Int.self)),
        GraphQLField("usernotes", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, assigned: String? = nil, beagonotes: String? = nil, due: String? = nil, householdid: String, name: String? = nil, status: Int? = nil, usernotes: String? = nil) {
        self.init(snapshot: ["__typename": "Task", "id": id, "assigned": assigned, "beagonotes": beagonotes, "due": due, "householdid": householdid, "name": name, "status": status, "usernotes": usernotes])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var assigned: String? {
        get {
          return snapshot["assigned"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "assigned")
        }
      }

      public var beagonotes: String? {
        get {
          return snapshot["beagonotes"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "beagonotes")
        }
      }

      public var due: String? {
        get {
          return snapshot["due"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "due")
        }
      }

      public var householdid: String {
        get {
          return snapshot["householdid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "householdid")
        }
      }

      public var name: String? {
        get {
          return snapshot["name"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }

      public var status: Int? {
        get {
          return snapshot["status"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "status")
        }
      }

      public var usernotes: String? {
        get {
          return snapshot["usernotes"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "usernotes")
        }
      }
    }
  }
}

public final class OnUpdateTaskSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnUpdateTask {\n  onUpdateTask {\n    __typename\n    id\n    assigned\n    beagonotes\n    due\n    householdid\n    name\n    status\n    usernotes\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onUpdateTask", type: .object(OnUpdateTask.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onUpdateTask: OnUpdateTask? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onUpdateTask": onUpdateTask.flatMap { $0.snapshot }])
    }

    public var onUpdateTask: OnUpdateTask? {
      get {
        return (snapshot["onUpdateTask"] as? Snapshot).flatMap { OnUpdateTask(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onUpdateTask")
      }
    }

    public struct OnUpdateTask: GraphQLSelectionSet {
      public static let possibleTypes = ["Task"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("assigned", type: .scalar(String.self)),
        GraphQLField("beagonotes", type: .scalar(String.self)),
        GraphQLField("due", type: .scalar(String.self)),
        GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(Int.self)),
        GraphQLField("usernotes", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, assigned: String? = nil, beagonotes: String? = nil, due: String? = nil, householdid: String, name: String? = nil, status: Int? = nil, usernotes: String? = nil) {
        self.init(snapshot: ["__typename": "Task", "id": id, "assigned": assigned, "beagonotes": beagonotes, "due": due, "householdid": householdid, "name": name, "status": status, "usernotes": usernotes])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var assigned: String? {
        get {
          return snapshot["assigned"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "assigned")
        }
      }

      public var beagonotes: String? {
        get {
          return snapshot["beagonotes"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "beagonotes")
        }
      }

      public var due: String? {
        get {
          return snapshot["due"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "due")
        }
      }

      public var householdid: String {
        get {
          return snapshot["householdid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "householdid")
        }
      }

      public var name: String? {
        get {
          return snapshot["name"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }

      public var status: Int? {
        get {
          return snapshot["status"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "status")
        }
      }

      public var usernotes: String? {
        get {
          return snapshot["usernotes"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "usernotes")
        }
      }
    }
  }
}

public final class OnDeleteTaskSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnDeleteTask {\n  onDeleteTask {\n    __typename\n    id\n    assigned\n    beagonotes\n    due\n    householdid\n    name\n    status\n    usernotes\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onDeleteTask", type: .object(OnDeleteTask.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onDeleteTask: OnDeleteTask? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onDeleteTask": onDeleteTask.flatMap { $0.snapshot }])
    }

    public var onDeleteTask: OnDeleteTask? {
      get {
        return (snapshot["onDeleteTask"] as? Snapshot).flatMap { OnDeleteTask(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onDeleteTask")
      }
    }

    public struct OnDeleteTask: GraphQLSelectionSet {
      public static let possibleTypes = ["Task"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("assigned", type: .scalar(String.self)),
        GraphQLField("beagonotes", type: .scalar(String.self)),
        GraphQLField("due", type: .scalar(String.self)),
        GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .scalar(String.self)),
        GraphQLField("status", type: .scalar(Int.self)),
        GraphQLField("usernotes", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, assigned: String? = nil, beagonotes: String? = nil, due: String? = nil, householdid: String, name: String? = nil, status: Int? = nil, usernotes: String? = nil) {
        self.init(snapshot: ["__typename": "Task", "id": id, "assigned": assigned, "beagonotes": beagonotes, "due": due, "householdid": householdid, "name": name, "status": status, "usernotes": usernotes])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var assigned: String? {
        get {
          return snapshot["assigned"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "assigned")
        }
      }

      public var beagonotes: String? {
        get {
          return snapshot["beagonotes"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "beagonotes")
        }
      }

      public var due: String? {
        get {
          return snapshot["due"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "due")
        }
      }

      public var householdid: String {
        get {
          return snapshot["householdid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "householdid")
        }
      }

      public var name: String? {
        get {
          return snapshot["name"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }

      public var status: Int? {
        get {
          return snapshot["status"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "status")
        }
      }

      public var usernotes: String? {
        get {
          return snapshot["usernotes"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "usernotes")
        }
      }
    }
  }
}

public final class OnCreatePersonSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnCreatePerson {\n  onCreatePerson {\n    __typename\n    id\n    householdid\n    name\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onCreatePerson", type: .object(OnCreatePerson.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onCreatePerson: OnCreatePerson? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onCreatePerson": onCreatePerson.flatMap { $0.snapshot }])
    }

    public var onCreatePerson: OnCreatePerson? {
      get {
        return (snapshot["onCreatePerson"] as? Snapshot).flatMap { OnCreatePerson(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onCreatePerson")
      }
    }

    public struct OnCreatePerson: GraphQLSelectionSet {
      public static let possibleTypes = ["Person"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, householdid: String, name: String? = nil) {
        self.init(snapshot: ["__typename": "Person", "id": id, "householdid": householdid, "name": name])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var householdid: String {
        get {
          return snapshot["householdid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "householdid")
        }
      }

      public var name: String? {
        get {
          return snapshot["name"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }
    }
  }
}

public final class OnUpdatePersonSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnUpdatePerson {\n  onUpdatePerson {\n    __typename\n    id\n    householdid\n    name\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onUpdatePerson", type: .object(OnUpdatePerson.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onUpdatePerson: OnUpdatePerson? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onUpdatePerson": onUpdatePerson.flatMap { $0.snapshot }])
    }

    public var onUpdatePerson: OnUpdatePerson? {
      get {
        return (snapshot["onUpdatePerson"] as? Snapshot).flatMap { OnUpdatePerson(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onUpdatePerson")
      }
    }

    public struct OnUpdatePerson: GraphQLSelectionSet {
      public static let possibleTypes = ["Person"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, householdid: String, name: String? = nil) {
        self.init(snapshot: ["__typename": "Person", "id": id, "householdid": householdid, "name": name])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var householdid: String {
        get {
          return snapshot["householdid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "householdid")
        }
      }

      public var name: String? {
        get {
          return snapshot["name"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }
    }
  }
}

public final class OnDeletePersonSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnDeletePerson {\n  onDeletePerson {\n    __typename\n    id\n    householdid\n    name\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onDeletePerson", type: .object(OnDeletePerson.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onDeletePerson: OnDeletePerson? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onDeletePerson": onDeletePerson.flatMap { $0.snapshot }])
    }

    public var onDeletePerson: OnDeletePerson? {
      get {
        return (snapshot["onDeletePerson"] as? Snapshot).flatMap { OnDeletePerson(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onDeletePerson")
      }
    }

    public struct OnDeletePerson: GraphQLSelectionSet {
      public static let possibleTypes = ["Person"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("householdid", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, householdid: String, name: String? = nil) {
        self.init(snapshot: ["__typename": "Person", "id": id, "householdid": householdid, "name": name])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var householdid: String {
        get {
          return snapshot["householdid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "householdid")
        }
      }

      public var name: String? {
        get {
          return snapshot["name"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }
    }
  }
}