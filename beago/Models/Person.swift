import Foundation
import AWSAppSync
import UIKit

public class Person {
    
    public var id: GraphQLID?
    public var householdid: String?
    public var name: String?
    
}
