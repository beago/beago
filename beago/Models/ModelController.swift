import AWSAppSync
import AWSMobileClient
import Foundation

public class ModelController {
    
    public static var _instance: ModelController? = nil
    
    public static var Instance: ModelController {
        get {
            if(_instance == nil) {
                _instance = ModelController()
            }
            return _instance!
        }
    }
    
    var appSyncClient: AWSAppSyncClient?
    public var tasks: [Task]?
    public var people: [Person]?
    public var househouldid: String?
    public var currentPerson: Person?
    
    
    
    public init() {
        Task.formatter.dateFormat = "dd-MM-yyyy"
        
        let databaseURL = URL(fileURLWithPath:NSTemporaryDirectory()).appendingPathComponent("beago")
        do {
            
            let appSyncConfig = try AWSAppSyncClientConfiguration(appSyncClientInfo: AWSAppSyncClientInfo(),databaseURL: databaseURL)
            appSyncClient = try AWSAppSyncClient(appSyncConfig: appSyncConfig)
        } catch {
            print("Error initializing appsync client. \(error)")
        }
    }
    
    public func loadAll(refreshable: Refreshable) {
        func afterLoadUser(person: Person) {
            self.househouldid = person.householdid
            self.loadTasks(refreshable: refreshable)
        }
        AWSMobileClient.sharedInstance().getIdentityId().continueWith { task in
            if let error = task.error {
                print("error: \(error.localizedDescription) \((error as NSError).userInfo)")
                return nil
            }
            else if let identityId = task.result {
                self.loadUser(identityId: identityId as String, resultTask: afterLoadUser)
            }
            return nil
        }
    }
    
    private func loadUser(identityId: String, resultTask: @escaping (Person) -> Void) {
        
        let query = ListPersonsQuery(limit: 100)
        query.filter = ModelPersonFilterInput()
        query.filter?.id = ModelIDFilterInput(eq: GraphQLID(identityId))
        appSyncClient?.fetch(query: query, cachePolicy: .fetchIgnoringCacheData) {(result, error) in
            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }
            let personItems = result?.data?.listPersons?.items!
            if (result?.data?.listPersons?.items != nil && (result?.data?.listPersons?.items?.count)! > 0) {
                self.people = result?.data?.listPersons?.items?.map ({ ModelTransform.transform($0!)})
                let person = ModelTransform.transform(personItems![0]!)
                resultTask(person)
            }
            else {
                self.createUser(identityId: identityId, resultTask: resultTask)
            }
        }
    }
    
    private func createUser(identityId: String, resultTask: @escaping (Person) -> Void) {
        let id = GraphQLID(identityId)
        let householdId = Unique.RandomId()
        let mutationInput = CreatePersonInput(id: id, householdid: householdId)
        
        appSyncClient?.perform(mutation: CreatePersonMutation(input: mutationInput)) { (result, error) in
            
            if let error = error as? AWSAppSyncClientError {
                print("Error occurred: \(error.localizedDescription )")
            }
            if let resultError = result?.errors {
                print("Error saving the item on server: \(resultError)")
                return
            }
            let person = ModelTransform.transform((result?.data?.createPerson)!)
            self.people = [person]
            resultTask(person)
        }
    }
    
    public func loadTasks(refreshable: Refreshable) {
        let query = ListTasksQuery(limit: 1000)
        query.filter = ModelTaskFilterInput()
        query.filter?.householdid = ModelStringFilterInput(eq: self.househouldid)
         appSyncClient?.fetch(query: query, cachePolicy: .fetchIgnoringCacheData) {(result, error) in
            if error != nil {
                print(error?.localizedDescription ?? "")
                self.tasks = []
                return
            }
            self.tasks = result?.data?.listTasks?.items?.map ({ ModelTransform.transform($0!)})
            refreshable.refresh()
        }
    }
    
    public func save(_ task: Task, resultTask: @escaping () -> Void) {
      //  let mutationInput = ModelTransform.transformUpdate(task)
        var mutationInput = UpdateTaskInput(id: task.id!, name: task.name)
        if let status = task.status  {
            mutationInput.status = status
        }
        if let due = task.due {
            mutationInput.due = due
        }
        
        appSyncClient?.perform(mutation: UpdateTaskMutation(input: mutationInput)) { (result, error) in
            
            if let error = error as? AWSAppSyncClientError {
                print("Error occurred: \(error.localizedDescription )")
                return
            }
            if let resultError = result?.errors {
                print("Error saving the item on server: \(resultError)")
                return
            }
            resultTask()
        }
    }
    
    public func save(_ person: Person, resultTask: @escaping () -> Void) {
        //  let mutationInput = ModelTransform.transformUpdate(task)
        let mutationInput = UpdatePersonInput(id: person.id!, name: person.name)
        appSyncClient?.perform(mutation: UpdatePersonMutation(input: mutationInput)) { (result, error) in
            
            if let error = error as? AWSAppSyncClientError {
                print("Error occurred: \(error.localizedDescription )")
                return
            }
            if let resultError = result?.errors {
                print("Error saving the item on server: \(resultError)")
                return
            }
            resultTask()
        }
    }

    public func createTask(resultTask: @escaping (Task) -> Void) {
        let id = GraphQLID(UUID().uuidString)
        let mutationInput = CreateTaskInput(id: id, householdid: self.househouldid!)
       
        appSyncClient?.perform(mutation: CreateTaskMutation(input: mutationInput)) { (result, error) in
           
            if let error = error as? AWSAppSyncClientError {
                print("Error occurred: \(error.localizedDescription )")
            }
            if let resultError = result?.errors {
                print("Error saving the item on server: \(resultError)")
                return
            }
            let t = ModelTransform.transform((result?.data?.createTask)!)
            self.tasks?.append(t)
            resultTask(t)
        }
    }
}
