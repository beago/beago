import Foundation

public class ModelTransform {
    
    public static func transform(_ input: ListTasksQuery.Data.ListTask.Item) -> Task {
        let result = Task()
        result.id = input.id
        result.assigned = input.assigned
        result.beagonotes = input.beagonotes
        result.due = input.due
        result.householdid = input.householdid
        result.name = input.name
        result.status = input.status
        result.usernotes = input.usernotes
        return result
    }
    public static func transform(_ input: CreateTaskMutation.Data.CreateTask) -> Task {
        let result = Task()
        result.id = input.id
        result.householdid = input.householdid
        return result
    }
    public static func transformUpdate(_ input: Task) -> UpdateTaskInput {
        let result = UpdateTaskInput(id: input.id!, assigned: input.assigned, beagonotes: input.beagonotes, due: input.due, householdid: input.householdid, name: input.name, status: input.status, usernotes: input.usernotes)
        
        return result
    }
    
    public static func transform(_ input: ListPersonsQuery.Data.ListPerson.Item) -> Person {
        let result = Person()
        result.id = input.id
        result.name = input.name
        result.householdid = input.householdid
        return result
    }
    public static func transform(_ input: CreatePersonMutation.Data.CreatePerson) -> Person {
        let result = Person()
        result.id = input.id
        result.householdid = input.householdid
        return result
    }
}
