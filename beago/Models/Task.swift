import Foundation
import AWSAppSync
import UIKit

public class Task {
    
    static var formatter = DateFormatter()
    
    public var id: GraphQLID?
    
    public var assigned: String?
    
    public var beagonotes: String?
    
    public var due: String?
    
    public var householdid: String?
    
    public var name: String?
    
    public var status: Int?
    
    public var usernotes: String?
    
    public func getDueDate() -> Date? {
        return due == nil ? nil : Task.formatter.date(from: due!)
    }
    public func setDueDate(dt: Date) {
         due = Task.formatter.string(from: dt)
    }
    
    func getOwnerImage() -> UIImage{
        return taskOwnerImage
    }
    public var taskOwnerImage: UIImage {
        get {
            return #imageLiteral(resourceName: "MoeJoePic.jpg")
        }
    }
    
}
