import UIKit

class HouseholdPersonCollectionViewCell: UICollectionViewCell {

    public var name: String!
    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        self.layer.cornerRadius = 24
        
        self.personImage.image = personImage.image?.withRenderingMode(.alwaysTemplate)
        self.personImage.tintColor = .white
        self.personImage.isHidden = name != nil
        if self.name != nil {
            
            self.lblName.text = String((name?.prefix(1))!)

        }
        
    }

}
