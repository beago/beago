import UIKit

class TaskEditViewController: UIViewController {
    
    public var refreshable: Refreshable!
    public var task: Task!
    @IBOutlet weak var txtDescription: UITextField!
    @IBOutlet weak var txtAssignedTo: UITextField!
    @IBOutlet weak var dueDate: UIDatePicker!
    @IBOutlet weak var btnStatus: UISegmentedControl!
    
    var setComplete: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let selected = self.task {
            txtDescription.text = selected.name
            txtAssignedTo.text = selected.assigned
            if(selected.getDueDate() != nil) {
                dueDate.date = selected.getDueDate()!
            }
            btnStatus.selectedSegmentIndex = selected.status ?? 0
        }
    }
    
    @IBAction func markComplete(_ sender: Any) {
        task.status = 1
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        task.name = txtDescription.text
        task.assigned = txtAssignedTo.text
        task.setDueDate(dt: dueDate.date)
        task.status = btnStatus.selectedSegmentIndex
        
        func refresh() {
            self.refreshable.refresh()
        }
        ModelController.Instance.save(task, resultTask: refresh)
    }
}
