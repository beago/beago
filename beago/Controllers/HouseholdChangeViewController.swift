import UIKit

class HouseholdChangeViewController: UIViewController {

    @IBOutlet weak var txtHouseHold: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txtHouseHold.text = ModelController.Instance.househouldid
    }

    @IBAction func loadClick(_ sender: Any) {
        ModelController.Instance.househouldid = txtHouseHold.text!
        
        self.tabBarController?.selectedIndex = 0
        let taskNav = self.tabBarController?.selectedViewController as! TaskNavigationController
        let taskList = taskNav.viewControllers[0] as! TaskListController
        
        ModelController.Instance.loadTasks(refreshable: taskList)

    }
}
