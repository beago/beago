import UIKit
import AWSAuthCore
import AWSAuthUI
import AWSMobileClient
import AWSAppSync

class HouseholdViewController:
    UIViewController,
    UICollectionViewDataSource,
    UICollectionViewDelegate,
    UIViewControllerTransitioningDelegate,
Refreshable {
 
    @IBOutlet weak var peopleView: UICollectionView!
    
  
    func openEdit(p: Person) {
        let editViewController: HouseholdPersonViewController = HouseholdPersonViewController()
        editViewController.person = p
        editViewController.refreshable = self
        self.navigationController?.pushViewController(editViewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        register()
        peopleView.dataSource = self
        peopleView.delegate = self
    
        let setHouseholdbtn = UIBarButtonItem(image:#imageLiteral(resourceName: "settings.png") , style: .done, target: self, action: #selector(setHousehold(_:)))
        self.navigationItem.rightBarButtonItem = setHouseholdbtn
    }
    
    @objc func setHousehold(_ sender:UIBarButtonItem) -> Void {
        
        let change = HouseholdChangeViewController()
        self.navigationController?.pushViewController(change, animated: true)
        
    }
    
    func refresh() {
        DispatchQueue.main.async {
            self.peopleView.reloadData()
        }
    }
    @IBAction func logout(_ sender: Any) {
        AWSMobileClient.sharedInstance().signOut()
        self.tabBarController?.selectedIndex = 0
        
        let taskNav = self.tabBarController?.selectedViewController as! TaskNavigationController
        let taskList = taskNav.viewControllers[0] as! TaskListController
        
        taskList.presentLogin()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard ModelController.Instance.people != nil  else {
            return 0;
        }
        return (ModelController.Instance.people?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HouseholdPersonCollectionViewCellIdentifier", for: indexPath) as! HouseholdPersonCollectionViewCell 
        let person = ModelController.Instance.people![indexPath.row]
        cell.name = person.name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let person = ModelController.Instance.people![indexPath.row]
        openEdit(p: person)
    }
    
    
    func register()  {
        let cellIdentifier = "HouseholdPersonCollectionViewCellIdentifier"
        let cellNib = UINib(nibName: "HouseholdPersonCollectionViewCell", bundle: nil)
        peopleView.register(cellNib, forCellWithReuseIdentifier: cellIdentifier)
    }
    
}
