import Foundation

public protocol Refreshable: class {
    func refresh()
}
