import UIKit

class HouseholdNavigationViewController: UINavigationController {

    @IBOutlet weak var navBar: UINavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        let vc = HouseholdViewController()
        self.pushViewController(vc, animated: false)
    }
}
