import UIKit
import AWSAuthCore
import AWSAuthUI
import AWSMobileClient
import AWSAppSync

class TaskListController:
    UIViewController ,
    UITableViewDataSource,
    UITableViewDelegate,
    Refreshable {
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var tableView: UITableView!

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard ModelController.Instance.tasks != nil  else {
            return 0;
        }
        return ModelController.Instance.tasks!.count
    }
    
    func refresh() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell")! as! TaskCellTableViewCell
        cell.task = ModelController.Instance.tasks![indexPath.row]
        cell.textLabel?.text = ModelController.Instance.tasks![indexPath.row].name // cell.task._name
        cell.SetData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let task = ModelController.Instance.tasks![indexPath.row]
        openEdit(t: task)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presentLogin()
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
    func presentLogin() {
        AWSMobileClient.sharedInstance().initialize { (userState, error) in
 
            if let us = userState {
                switch(us){
                case .signedIn:
                    DispatchQueue.main.async {
                        print("logged in")
                    }
                    ModelController.Instance.loadAll(refreshable: self)
                case .signedOut,
                     .guest:
                    
                    let options = SignInUIOptions(canCancel: false, logoImage: #imageLiteral(resourceName: "checkhouselogin.png"), backgroundColor: UIColor.white)
                    
                    AWSMobileClient.sharedInstance().showSignIn(
                        navigationController: self.navigationController!,
                        signInUIOptions: options, { (userState, error) in
                        if(error == nil){       //Successful signin
                            DispatchQueue.main.async {
                                print("Logged in")
                            }
                            ModelController.Instance.loadAll(refreshable: self)
                        }
                    })
                default:
                    AWSMobileClient.sharedInstance().signOut()
                }
                
            } else if let error = error {
                print(error.localizedDescription)
            }
        }
    }
    
    @IBAction func addClick(_ sender: Any) {
        ModelController.Instance.createTask(resultTask: openEdit)
    }
    
    func openEdit(t: Task) {
        let taskEditViewController: TaskEditViewController = TaskEditViewController()
        taskEditViewController.task = t
        taskEditViewController.refreshable = self
        self.navigationController?.pushViewController(taskEditViewController, animated: true)
    }
    
    @IBAction func reloadGrid(_ sender: Any) {
        ModelController.Instance.loadAll(refreshable: self)
    }
    
}

