import UIKit
import AWSAuthCore
import AWSAuthUI
import AWSMobileClient
import AWSAppSync

class TaskNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logoutBtn = UIBarButtonItem(title: "log out", style: .done, target: self, action: #selector(logout(_:)))
        
        self.navigationItem.rightBarButtonItem = logoutBtn
    }
    
    @objc func logout(_ sender:UIBarButtonItem) -> Void {
        
        AWSMobileClient.sharedInstance().signOut()
        
        self.navigationController?.popToRootViewController(animated: true)
    }
}
