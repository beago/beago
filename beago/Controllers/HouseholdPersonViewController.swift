import UIKit

class HouseholdPersonViewController: UIViewController {

    @IBOutlet weak var txtName: UITextField!
    
    var person: Person!
    var refreshable: Refreshable!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let selected = self.person {
            txtName.text = selected.name
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        person.name = txtName.text
        
        func refresh() {
            self.refreshable.refresh()
        }
        
        ModelController.Instance.save(self.person, resultTask: refresh)
    }
    
}
